<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas Pekan 3 Hari 2 - Laravel</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <!--action menunjukan halaman mana yang akan di tujuh setelah di klik sign up-->
    <!-- pada tag input, select, option textarea tidak menggunakan id / name karena 
        tidak ada menghasilkan inputan yang akan muncul pada halaman selanjutnya
    -->
    <form action="/welcome" method="post">
        @csrf
        <label>Fist name:</label>  </p> 
        <input type="text" name="nama"> </p>

        <label>Last name:</label>  </p> 
        <input type="text"> </p>

        <label>Gender:</label>  </p> 
        <input type="radio">Male </br>
        <input type="radio">Female </br>
        <input type="radio">Other </p>

        <label>Nationality:</label>  </p> 
        <select>
            <option>Indonesia</option>
            <option>Inggris</option>
        </select> </p>

        <label>Language Spoken:</label>  </p> 
        <input type="checkbox">Bahasa Indonesia </br>
        <input type="checkbox">English </br>
        <input type="checkbox">Other </p>

        <label>Bio:</label>  </p> 
        <textarea rows="10" cols="30"> </textarea> </br>

        <input type="submit" value="Sign Up">

    </form>
    
</body>
</html>