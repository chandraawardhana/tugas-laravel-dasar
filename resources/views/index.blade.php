<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas Pekan 3 Hari 2 - Laravel </title>
</head>
<body>
    <!-- Menggunakan tag <h1> untuk menampilkan tulisan dengan ukuran terbesar dan menggunakan tag bold dengan tag <b> -->
    <h1> <b> SanberBook </b> </h1> 

    <!-- Menggunakan tag <h2> untuk menampilkan tulisan dengan ukuran lebih kecil dari h1 dan menggunakan tag bold dengan tag <b> -->
    <h2><b> Social Media Developer Santai Berkualitas </b> </h2>
    
    <!-- Menggunakan tag paragraf untuk membuat tulisan dan memberikan jarak -->
    <p> Belajar dan Berbagi agar hidup ini semakin santai berkualitas </p> 
    
    <!-- Menggunakan tag <h3> untuk menampilkan tulisan dengan ukuran lebih kecil dari h2 dan menggunakan tag bold dengan tag <b> -->
    <h3><b>Benefit Join di SanberBook</b></h3>
    <!-- Menggunakan tag <ul> untuk unordered list -->
    <ul>
        <li>Mendapatkan motivasi dari sesama developer</li>
        <li>Sharing knowledge dari para mastah Sanber</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    
     <!-- Menggunakan tag <h3> untuk menampilkan tulisan dengan ukuran lebih kecil dari h2 dan menggunakan tag bold dengan tag <b> -->
    <h3><b>Cara Bergabung ke SanberBook</b></h3>
    <!-- Menggunakan tag <ol> untuk ordered list -->
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftar di <a href="{{url('register')}}">Form Sign Up </a> </li>
        <li>Selesai!</li>
    </ol>

</body>
</html>